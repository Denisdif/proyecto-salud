<?php

namespace App\Http\Controllers;

use App\AntecedenteReciente;
use Illuminate\Http\Request;

class AntecedenteRecienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AntecedenteReciente  $antecedenteReciente
     * @return \Illuminate\Http\Response
     */
    public function show(AntecedenteReciente $antecedenteReciente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AntecedenteReciente  $antecedenteReciente
     * @return \Illuminate\Http\Response
     */
    public function edit(AntecedenteReciente $antecedenteReciente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AntecedenteReciente  $antecedenteReciente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AntecedenteReciente $antecedenteReciente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AntecedenteReciente  $antecedenteReciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(AntecedenteReciente $antecedenteReciente)
    {
        //
    }
}
